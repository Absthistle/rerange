package com.puradox.rerange.rerange;

import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = Rerange.MOD_ID)
@Config.LangKey("rerange.config.title")
public class ModConfig {
    @Config.LangKey("rerange.config.dist")
    public static DistCategory distCat;
    @Config.LangKey("rerange.config.cap")
    public static CapCategory capCat;

    public static class DistCategory {
        @Config.LangKey("rerange.config.dist.min_distance")
        @Config.Comment("The minimum distance from which monsters can spawn from the player. 0-23 will only function in the presence of a similar mod, or while in-bounds teleportation is active.")
        @Config.RangeInt(min = 0, max = 128)
        public static int min_spawn_distance = 24;
        @Config.LangKey("rerange.config.dist.max_distance")
        @Config.Comment("The maximum distance from which monsters can spawn from the player.")
        @Config.RangeInt(min = 0, max = 128)
        public static int max_spawn_distance = 128;

        @Config.LangKey("rerange.config.dist.ignore_chance")
        @Config.Comment("Chance to ignore set maximum distance and spawn anyway.")
        @Config.RangeInt(min = 0, max = 100)
        public static int ignore_chance = 0;

        @Config.LangKey("rerange.config.dist.retrieve")
        @Config.Comment("Teleport monsters within maximum spawn distance, instead of denying spawn. May cause strange spawning behaviour and slightly more lag.")
        public static boolean retrieve = false;

        @Config.LangKey("rerange.config.dist.whitelist")
        @Config.Comment("Dimensions to whitelist; others will follow normal spawning patterns.")
        public static int[] dimWhite = new int[]{-1,0,1};
    }

    public static class CapCategory {
        @Config.LangKey("rerange.config.cap.monster_cap")
        @Config.RangeInt(min = -1)
        public static int monsterCap = 70;

        @Config.LangKey("rerange.config.cap.creature_cap")
        @Config.RangeInt(min = -1)
        public static int creatureCap = 10;

        @Config.LangKey("rerange.config.cap.ambient_cap")
        @Config.RangeInt(min = -1)
        public static int ambientCap = 15;

        @Config.LangKey("rerange.config.cap.water_cap")
        @Config.RangeInt(min = -1)
        public static int waterCap = 5;
    }

    @Mod.EventBusSubscriber(modid = Rerange.MOD_ID)
    private static class EventHandler {

        @SubscribeEvent
        public static void onConfigChanged(final ConfigChangedEvent.OnConfigChangedEvent event) {
            if (event.getModID().equals(Rerange.MOD_ID)) {
                ConfigManager.sync(Rerange.MOD_ID, Config.Type.INSTANCE);
                Rerange.assignEntityCaps();
            }
        }
    }
}
