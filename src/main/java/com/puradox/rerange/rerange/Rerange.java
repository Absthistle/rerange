package com.puradox.rerange.rerange;

import net.minecraft.entity.EnumCreatureType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

@Mod(
        modid = Rerange.MOD_ID,
        name = Rerange.MOD_NAME,
        version = Rerange.VERSION,
        acceptableRemoteVersions = "*"
)
public class Rerange {

    public static final String MOD_ID = "rerange";
    public static final String MOD_NAME = "Rerange";
    public static final String VERSION = "1.2.0";


    @Mod.Instance(MOD_ID)
    public static Rerange INSTANCE;

    @Mod.EventHandler
    public void preinit(FMLPreInitializationEvent event) {
        MinecraftForge.EVENT_BUS.register(EventHandler.class);

        assignEntityCaps();
    }

    public static void assignEntityCaps() {
        try {
            Field mobCap = ObfuscationReflectionHelper.findField(EnumCreatureType.class, "field_75606_e");
            mobCap.setAccessible(true);

            Field modifierField = Field.class.getDeclaredField("modifiers");
            modifierField.setAccessible(true);
            modifierField.setInt(mobCap, mobCap.getModifiers() & ~Modifier.FINAL);

            mobCap.setInt(EnumCreatureType.MONSTER, ModConfig.CapCategory.monsterCap);
            mobCap.setInt(EnumCreatureType.CREATURE, ModConfig.CapCategory.creatureCap);
            mobCap.setInt(EnumCreatureType.AMBIENT, ModConfig.CapCategory.ambientCap);
            mobCap.setInt(EnumCreatureType.WATER_CREATURE, ModConfig.CapCategory.waterCap);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
