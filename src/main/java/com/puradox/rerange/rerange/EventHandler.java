package com.puradox.rerange.rerange;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.init.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldEntitySpawner;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import net.minecraftforge.fml.common.eventhandler.Event;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import java.util.Objects;
import java.util.Random;

@Mod.EventBusSubscriber
@Cancelable
public class EventHandler {
    public static Random rand = new Random();

    @SubscribeEvent
    public static void livingSpawn(LivingSpawnEvent.CheckSpawn event) {
        EntityLivingBase entity = event.getEntityLiving();
        World world = entity.getEntityWorld();

        if (inWhiteList(entity.dimension) && (entity instanceof IMob) && !event.isSpawner() && !entity.forceSpawn) {
            if((world.getClosestPlayerToEntity(entity, ModConfig.DistCategory.max_spawn_distance) == null
                    || event.getWorld().getClosestPlayerToEntity(entity, ModConfig.DistCategory.min_spawn_distance) != null)
            && world.getClosestPlayerToEntity(entity, Integer.MAX_VALUE) != null) {
                if(rand.nextInt(100) >= ModConfig.DistCategory.ignore_chance) {
                    if(ModConfig.DistCategory.retrieve) {
                        BlockPos playerPos = Objects.requireNonNull(world.getClosestPlayerToEntity(entity, Integer.MAX_VALUE)).getPosition();
                        for(int i=0;i<=5; i++) { // 6 attempts to redetermine spawn.

                            int[] negatives = new int[3];
                            for (int n = 0; n <= 2; n++) {
                                negatives[n] = (rand.nextInt(2) * 2) - 1;
                            } //Three negatives, for each axis.


                            int spawnDistX = rand.nextInt(ModConfig.DistCategory.max_spawn_distance-1)+1;
                            int spawnDistZ = rand.nextInt(ModConfig.DistCategory.max_spawn_distance-1)+1;
                            if(spawnDistX<ModConfig.DistCategory.min_spawn_distance && spawnDistZ<ModConfig.DistCategory.min_spawn_distance) {
                                double deviation = (double)ModConfig.DistCategory.min_spawn_distance/Math.max(spawnDistX, spawnDistZ);
                                spawnDistX*=deviation;
                                spawnDistZ*=deviation;
                            } //If anything attempts to spawn inside minimum distance, place it within minimum distance.

                            BlockPos newPos = new BlockPos(
                                    playerPos.getX() + (spawnDistX * negatives[0]),
                                    playerPos.getY() + (Math.min(40, rand.nextInt(ModConfig.DistCategory.max_spawn_distance)) * negatives[1]),
                                    playerPos.getZ() + (spawnDistZ * negatives[2])
                            ); //Attempted new spawn position.


                            Chunk chunk = world.getChunk(newPos);
                            int attempts = 0;
                            if(Block.isEqualTo(world.getBlockState(entity.getPosition().down()).getBlock(), Blocks.WATER)) {
                                while (!Block.isEqualTo(world.getBlockState(newPos.down()).getBlock(), Blocks.WATER) && attempts < 15) {
                                    newPos = newPos.down();
                                    attempts++;
                                }
                                IBlockState state = chunk.getBlockState(newPos);
                                if(state.getLightValue(world, newPos)==0 && WorldEntitySpawner.canCreatureTypeSpawnBody(EntityLiving.SpawnPlacementType.IN_WATER, world, newPos)) {
                                    entity.setPosition(newPos.getX(), newPos.getY(), newPos.getZ());
                                    event.setResult(Event.Result.ALLOW); //Spawn if valid spawn loc. Job complete.
                                    return;
                                } //Spawn if valid water spawn loc.
                            } else {
                                while (world.isAirBlock(newPos.down()) && attempts < 20) {
                                    newPos = newPos.down();
                                    attempts++;
                                }
                                IBlockState state = chunk.getBlockState(newPos.down());
                                if(state.getLightValue(world, newPos.down())==0 && WorldEntitySpawner.canCreatureTypeSpawnBody(EntityLiving.SpawnPlacementType.IN_AIR, world, newPos)) {
                                    entity.setPosition(newPos.getX(), newPos.getY(), newPos.getZ());
                                    event.setResult(Event.Result.ALLOW);
                                    return;
                                } //Spawn if valid ground/air spawn loc.
                            } //Spawn upon the next Y of ground/water block available, preferrably not in the air.
                        }
                        event.setResult(Event.Result.DENY); //If retrieval succeeded, the result should already be allowed, and return should be called prior.
                    } else {event.setResult(Event.Result.DENY);} //Deny if outside valid spawn area. If retrieval isn't enabled, job is complete.
                }
            }
        }
    }

    //Determine if current dimension adheres to whitelist.
    public static boolean inWhiteList(int dimension) {
        for (int dim : ModConfig.DistCategory.dimWhite) {
            if (dimension == dim) {
                return true;
            }
        }
        return false;
    }
}
