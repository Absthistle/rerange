# Rerange

## About

Rerange implements the ability to forcibly modify all mob caps and monster spawn distance from player, through its configuration menu. It functions by denying spawns, or if a particular setting is enabled, teleporting out-of-bounds spawns to the preferred range.

## Cubic Chunks compatibility

Rerange is designed to be compatible with Cubic Chunks and Cubic World Gen. If any issues arise in relation to said mods, please report them here.

## 1.13+ port?

This mod was especially developed to fill the void of any such mods for 1.12.2. As there are already mods available for this purpose in later versions (see: InControl 1.13+ for a general implementation), this mod will not receive updates for later versions of Minecraft.
